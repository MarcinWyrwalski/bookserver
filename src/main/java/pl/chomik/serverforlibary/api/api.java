package pl.chomik.serverforlibary.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.chomik.serverforlibary.domain.Book;
import pl.chomik.serverforlibary.service.BookManager;

import java.util.List;

@RestController
public class api {

    @Autowired
    BookManager bookManager;

   @PostMapping("/addBook")
    public boolean addBook (@RequestBody Book book){
        return bookManager.addBook(book);
   }

   @GetMapping("/getbook")
    public List<Book> getBook(){
       return bookManager.getBooksList();
   }



}
