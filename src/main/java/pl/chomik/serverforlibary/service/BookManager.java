package pl.chomik.serverforlibary.service;

import org.springframework.stereotype.Service;
import pl.chomik.serverforlibary.domain.Book;

import java.util.List;

@Service
public class BookManager {

    private List<Book> books;

    public BookManager(List<Book> books) {
        this.books = books;
        books.add(new Book("tym", "tom"));
        books.add(new Book("rym", "rom"));
        books.add(new Book("uoi", "zom"));
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public boolean addBook(Book book){
        return books.add(book);
    }

    public List<Book> getBooksList() {
        return books;
    }
}


