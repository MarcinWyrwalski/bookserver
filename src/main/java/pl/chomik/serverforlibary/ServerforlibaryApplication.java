package pl.chomik.serverforlibary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerforlibaryApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerforlibaryApplication.class, args);
    }

}
